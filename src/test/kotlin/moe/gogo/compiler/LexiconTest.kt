package moe.gogo.compiler

import io.kotlintest.matchers.shouldBe
import io.kotlintest.specs.StringSpec

class LexiconTest : StringSpec() {

    init {
        "define"{
            val lexion = Lexicon()

            val regex = Regex(".*")
            val t1 = lexion.define("t1", regex)
            val t2 = lexion.defineName("t2", "TName", regex)
            val t3 = lexion.define("t3", regex)
            val t4 = lexion.define("t4", regex)

            val list = lexion.tokens
            list.size shouldBe 4
            list[0] shouldBe t1
            list[1] shouldBe t2
            list[2] shouldBe t3
            list[3] shouldBe t4
        }
    }

}