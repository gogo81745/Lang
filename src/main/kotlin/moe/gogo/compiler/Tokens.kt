package moe.gogo.compiler

object Tokens {
    val LEXICON = Lexicon()

    val IF: Token
    val ELSE: Token
    val FOR: Token

    val AND: Token
    val OR: Token
    val NOT: Token
    val LESS_EQ: Token
    val LESS: Token
    val GREATER_EQ: Token
    val GREATER: Token
    val EQUAL: Token
    val ASSIGN :Token
    val PLUS: Token
    val MINUS: Token
    val TIMES: Token
    val DIV: Token
    val LEFT_PH: Token
    val RIGHT_PH: Token
    val LEFT_BK: Token
    val RIGHT_BK: Token
    val LEFT_BR: Token
    val RIGHT_BR: Token
    val DOT: Token
    val COMMA: Token
    val COLON: Token
    val SEMICOLON: Token

    val COMMENTS: Token
    val MULTILINE_COMMENTS: Token
    val DOCUMENT_COMMENTS: Token

    val NUMBER: Token
    val ID: Token


    init {
        IF = LEXICON.define("If", Regex("""if"""))
        ELSE = LEXICON.define("Else", Regex("""else"""))
        FOR = LEXICON.define("For", Regex("""for"""))

        AND = LEXICON.define("And", Regex("""&&"""))
        OR = LEXICON.define("Or", Regex("""\|\|"""))
        NOT = LEXICON.define("Not", Regex("""!"""))
        LESS_EQ = LEXICON.define("LessEqual", Regex("""<="""))
        LESS = LEXICON.define("Less", Regex("""<"""))
        GREATER_EQ = LEXICON.define("GreaterEqual", Regex(""">="""))
        GREATER = LEXICON.define("Greater", Regex(""">"""))
        EQUAL = LEXICON.define("Equal", Regex("""=="""))
        ASSIGN = LEXICON.define("Assign", Regex("""="""))
        PLUS = LEXICON.define("Plus", Regex("""\+"""))
        MINUS = LEXICON.define("Minus", Regex("""-"""))
        TIMES = LEXICON.define("Times", Regex("""\*"""))
        DIV = LEXICON.define("Div", Regex("""/"""))
        LEFT_PH = LEXICON.define("LeftPH", Regex("""\("""))
        RIGHT_PH = LEXICON.define("RightPH", Regex("""\)"""))
        LEFT_BK = LEXICON.define("LeftBK", Regex("""\["""))
        RIGHT_BK = LEXICON.define("RightBK", Regex("""]"""))
        LEFT_BR = LEXICON.define("LeftBR", Regex("""\{"""))
        RIGHT_BR = LEXICON.define("RightBR", Regex("""}"""))
        DOT = LEXICON.define("Dot", Regex("""\."""))
        COMMA = LEXICON.define("Comma", Regex(""","""))
        COLON = LEXICON.define("Colon", Regex(""":"""))
        SEMICOLON = LEXICON.define("Semicolon", Regex(""";"""))

        COMMENTS = LEXICON.defineMultiWords("Comments", Regex("""//"""), Regex("""\n"""))
        DOCUMENT_COMMENTS = LEXICON.defineMultiWords("DocumentComments", Regex("""/\*\*"""), Regex("""\*/"""))
        MULTILINE_COMMENTS = LEXICON.defineMultiWords("MultiLineComments",  Regex("""/\*"""), Regex("""\*/"""))

        NUMBER = LEXICON.defineArgument("Number", Regex("""\d+"""))
        ID = LEXICON.defineArgument("Id", Regex("""[\p{Lu}\p{Ll}\p{Lt}\p{Lm}\p{Lo}\p{Nl}][\p{Lu}\p{Ll}\p{Lt}\p{Lm}\p{Lo}\p{Nl}\p{Mn}\p{Mc}\p{Nd}\p{Pc}\p{Cf}]*"""))

    }
}
