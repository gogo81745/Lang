package moe.gogo.compiler

import java.util.Deque

abstract class Token(val id: String, val regex: Regex) {

    abstract fun toLexeme(string: String): Lexeme

    open fun matches(string: String): Boolean = regex.matches(string)

    open fun putLexeme(list: MutableCollection<Lexeme>, deque: Deque<String>, string: String) {
        list.add(toLexeme(string))
    }

    override fun toString(): String {
        return id
    }

}

class NameToken(id: String, val name: String, regex: Regex) : Token(id, regex) {

    override fun toLexeme(string: String): Lexeme = NameLexeme(this)

    override fun toString(): String {
        return name
    }

}

class ArgumentToken(id: String, regex: Regex) : Token(id, regex) {

    override fun toLexeme(string: String): Lexeme = ArgumentLexeme(this, string)

}

class MultiWordsToken(
        id: String,
        private val start: Regex,
        private val end: Regex) : Token(id, Regex(start.pattern + ".*")) {

    override fun toLexeme(string: String): Lexeme = ArgumentLexeme(this, string)

    override fun putLexeme(list: MutableCollection<Lexeme>, deque: Deque<String>, string: String) {
        val builder = StringBuilder()

        handleBehindStartString(string, builder)

        while (!deque.isEmpty()) {
            val it = deque.pop()
            if (handleStop(it, builder, deque, list)) {
                return
            }
            builder.append(it)
        }

    }

    private fun handleBehindStartString(string: String, builder: StringBuilder) {
        val behind = string.replace(start, "")
        if (behind.isNotEmpty()) {
            builder.append(behind)
        }
    }

    private fun handleStop(
            it: String,
            builder: StringBuilder,
            deque: Deque<String>,
            list: MutableCollection<Lexeme>): Boolean {

        if (endMatcher.matches(it)) {
            handleStopRemindString(it, builder, deque)
            list.add(toLexeme(builder.toString()))
            return true
        }
        return false
    }

    val endMatcher = Regex(".*${end.pattern}.*")

    private fun handleStopRemindString(it: String, builder: StringBuilder, deque: Deque<String>) {
        val remind = it.split(end)
        if (remind[0].isNotEmpty()) {
            builder.append(remind[0])
        }
        if (remind[1].isNotEmpty()) {
            deque.push(remind[1])
        }
    }
}