package moe.gogo.compiler

class Lexicon {

    private val tokensField = mutableListOf<Token>()

    val tokens: List<Token> = tokensField

    fun define(id: String, regex: Regex): Token = defineName(id, id, regex)

    fun defineName(id: String, name: String, regex: Regex): Token {
        val token = NameToken(id, name, regex)
        tokensField.add(token)
        return token
    }

    fun defineArgument(id: String, regex: Regex): Token {
        val token = ArgumentToken(id, regex)
        tokensField.add(token)
        return token
    }

    fun defineMultiWords(id: String, start: Regex, end: Regex): Token {
        val token = MultiWordsToken(id, start, end)
        tokensField.add(token)
        return token
    }
}